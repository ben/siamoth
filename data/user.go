package data

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// BCryptCost is a number representing the time cost of password hashing.
const BCryptCost = bcrypt.DefaultCost

// User represents an account in Siamoth.
type User struct {
	gorm.Model
	LoginName string `gorm:"unique_index"`
	Password  []byte
}

// SetPassword changes the user's stored password.
// The record is not automatically updated in the database.
func (u *User) SetPassword(password string) error {
	p, err := bcrypt.GenerateFromPassword([]byte(password), BCryptCost)
	if err == nil {
		u.Password = p
	}

	return errors.Wrap(err, "setting user's password")
}

// CheckPassword verifies that the user's stored password matches the entered password.
// If update is true, the password was re-hashed due to a changed BCryptCost parameter.
func (u *User) CheckPassword(password string) (valid, update bool, err error) {
	err = bcrypt.CompareHashAndPassword(u.Password, []byte(password))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		err = nil
		return
	}

	if err == nil {
		valid = true

		var cost int
		cost, err = bcrypt.Cost(u.Password)

		if err == nil && cost < BCryptCost {
			err = u.SetPassword(password)
			update = true
		}
	}

	err = errors.Wrap(err, "checking password")

	return
}
