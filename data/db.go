// Package data is the data access layer for Siamoth.
package data

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	// PostgreSQL driver
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB //nolint:gochecknoglobals

// DB returns the database.
//
// This method panics if Init was not previously called.
func DB() *gorm.DB {
	if db == nil {
		panic(errors.New("siamoth/data: Init must be called before DB is called"))
	}

	return db.New()
}

// Init establishes the global database connection.
func Init(dataSource string) (err error) {
	if db != nil {
		return errors.New("siamoth/data: database is already initialized")
	}

	db, err = gorm.Open("postgres", dataSource)
	if err != nil {
		return errors.Wrap(err, "connecting to database")
	}

	err = errors.Wrap(db.AutoMigrate(
		&User{},
	).Error, "initializing database")

	return
}
