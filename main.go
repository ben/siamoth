// Siamoth is a self-hosted web-based personal organizer.
package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"runtime"

	"git.lubar.me/ben/siamoth/data"
	"git.lubar.me/ben/siamoth/plugins"
	"git.lubar.me/ben/siamoth/version"
	"git.lubar.me/ben/siamoth/web"
)

func main() {
	flag.Usage = usage
	flagDataSource := flag.String("db", "", "PostgreSQL connection string")
	flagHTTPAddr := flag.String("http", ":8008", "The listen address for the server.")
	flagVersion := flag.Bool("version", false, "show version information, then exit")

	flag.Parse()

	if *flagVersion {
		showVersion()
		return
	}

	if *flagDataSource == "" {
		startupError("Missing required -db flag.")
	}

	if err := data.Init(*flagDataSource); err != nil {
		startupError("Could not connect to database: %+v", err)
	}

	if err := plugins.Init(); err != nil {
		startupError("Error initializing plugins: %+v", err)
	}

	if err := http.ListenAndServe(*flagHTTPAddr, web.Router()); err != nil {
		startupError("Could not start HTTP listener: %+v", err)
	}
}

func usage() {
	_, _ = fmt.Fprintf(flag.CommandLine.Output(), "Usage for %s:\n", os.Args[0])
	flag.PrintDefaults()
}

func showVersion() {
	fmt.Printf("Siamoth Version: %s (build %s)\n", version.Release, version.Build)
	fmt.Printf("Code version: %s\n", version.Commit)
	fmt.Printf("Go version: %s\n", runtime.Version())
	fmt.Printf("OS: %s\n", runtime.GOOS)
	fmt.Printf("Architecture: %s\n", runtime.GOARCH)
}

func startupError(message string, args ...interface{}) {
	_, _ = fmt.Fprintf(flag.CommandLine.Output(), message, args...)
	_, _ = io.WriteString(flag.CommandLine.Output(), "\n\n")

	usage()
	os.Exit(1)

	panic("unreachable")
}
