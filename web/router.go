// Package web contains the Siamoth web router.
package web

import "github.com/gorilla/mux"

var router = mux.NewRouter() //nolint:gochecknoglobals

// Router is the Siamoth web router.
func Router() *mux.Router {
	return router
}
