# Siamoth

Siamoths are a type of porcine primarily found in humid or temperate regions throughout Tyria. They resemble other odd-toed ungulates, of which Tapirs are a notable member. Siamoths, along with cats, are the only known creatures in-game with textured anuses.

*(from [the Guild Wars 2 official wiki](https://wiki.guildwars2.com/wiki/Siamoth))*

# Siamoth

[![latest build status icon](https://buildmaster.lubar.me/api/ci-badges/image?key=badges&$ApplicationName=Siamoth&$TrustedBuild=true)](https://buildmaster.lubar.me/api/ci-badges/link?key=badges&$ApplicationName=Siamoth&$TrustedBuild=true) [![Go reference documentation](https://godoc.org/git.lubar.me/ben/siamoth?status.svg)](https://godoc.org/git.lubar.me/ben/siamoth) [![Go report card](https://goreportcard.com/badge/git.lubar.me/ben/siamoth)](https://goreportcard.com/report/git.lubar.me/ben/siamoth)

This program is also named Siamoth, and is a personal organizer.

## Installation

Siamoth can be installed using the usual commands:

```bash
git clone https://git.lubar.me/ben/siamoth.git
cd siamoth
go install
```

## Usage

*This project is still in early planning stages and cannot yet be used.*

## Contributing

Pull requests and issue reports are encouraged. For pull requests that change more than a few lines, please open or find an issue first to avoid duplicated work.

## License

Siamoth is licensed under the [Cooperative Software License (CSL)](https://csl.lubar.me/). A copy of the license is included in this repository, and several web mirrors are available.
