module git.lubar.me/ben/siamoth

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/pkg/errors v0.8.1
	golang.org/x/crypto v0.0.0-20191108234033-bd318be0434a
)
