// Package version is automatically replaced by the build script.
package version

// Constants representing the version of Siamoth that was built.
const (
	Release = "[custom build]"
	Build   = "[custom build]"
	Commit  = "[unknown]"
)
